from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('comment.urls')),
    path('aboutme/', include('aboutme.urls')),
    path('mybooks/', include('mybooks.urls')),
    path('user/', include('login.urls')),
]
