from django.shortcuts import render, redirect
from .models import Comment
from .forms import CommentForm

def home(request):
    comments = Comment.objects.all().order_by('-created_at')
    comments_form = CommentForm(request.POST or None)

    if request.method == "POST":
        if comments_form.is_valid():
            comments_form.save()

            return redirect('home')

    context = {
        'judul' : 'CommentMee',
        'comments' : comments,
        'comments_form' : comments_form
    }

    return render(request, 'comment/comment.html', context)
