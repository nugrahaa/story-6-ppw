from django.db import models

class Comment(models.Model):
    comment = models.TextField(max_length=300, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.comment
    
