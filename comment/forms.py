from django import forms

from .models import Comment

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = [
            'comment',
        ]

        widgets = {
            'comment' : forms.Textarea (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'ceritakan kesantuyan anda hari ini ...',
                    'cols' : 50,
                    'rows' : 5,
                }
            ),
        }