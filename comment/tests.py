from django.test import TestCase
from django.conf import settings
from importlib import import_module
from .views import home
from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from .models import Comment
from .forms import CommentForm

# FOR SELENIUM
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

class PepewUnitTest(TestCase):

    # URL TEST 
    def test_landing_page_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_url_not_found(self):
        response = Client().get('/agan/')
        self.assertEqual(response.status_code, 404)

    # FUNCTION TEXT
    def test_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    # TEMPLATE USE TEST
    def test_landing_page_uses_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'comment/comment.html')
    
    # TEXT TEST
    def test_landing_page_content_has_content(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Halo, Apa Kabar?", html_response)

    # MODEL TEST
    def test_model_can_create_new_comment(self):
        Comment.objects.create(
            comment = 'Minggu ini santuy parah',
            created_at = timezone.now()
        )

        count_all_comment = Comment.objects.all().count()
        self.assertEqual(count_all_comment, 1)

    def test_feedback_creation(self):
        obj = Comment.objects.create(
            comment = 'PPW ez',
            created_at = timezone.now()
        )

        self.assertTrue(isinstance(obj, Comment))
        self.assertEqual(obj.__str__(), obj.comment)

    # FORM TEST
    def test_valid_form(self):
        obj = Comment.objects.create(
            comment = 'SDA PPW sans',
            created_at = timezone.now()
        )
        data = {
            'comment' : obj.comment,
            'created_at' : obj.created_at,
        }
        form = CommentForm(data=data)
        self.assertTrue(form.is_valid())
    
    def test_invalid_form(self):
        obj = Comment.objects.create(
            comment = '',
            created_at = 123
        )
        data = {
            'comment' : obj.comment,
            'created_at' : obj.created_at,
        }
        form = CommentForm(data=data)
        self.assertFalse(form.is_valid())

class CommentFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(CommentFunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(CommentFunctionalTest, self).tearDown()

    def test_input_comment(self):
        selenium = self.selenium
        # OPEN THE LINK
        selenium.get('http://127.0.0.1:8000/')
        # FIND ELEMEN
        comment = selenium.find_element_by_id('id_comment')
        submit = selenium.find_element_by_class_name('tombol')
        # INPUT
        comment.send_keys('Santuy adalah jalan ninjaku')
        time.sleep(1)
        submit.send_keys(Keys.RETURN)
    
    def test_title_is_exist(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        header = selenium.find_element_by_class_name('title')
        self.assertEqual(header.text, 'Halo, Apa Kabar?')
    
    def test_title_text_logo_is_exist(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        title = selenium.find_element_by_class_name('text-santuy')
        self.assertEqual(title.text, 'SANTUY')

        
