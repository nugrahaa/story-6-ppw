from django.test import TestCase
from django.conf import settings
from importlib import import_module
from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.utils import timezone
from django.contrib.auth.models import User

from .views import loginView

# FOR SELENIUM
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

class LoginUnitTest(TestCase):

    # URL TEST 
    def test_login_page_exist(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_url_not_found(self):
        response = Client().get('/hehe/')
        self.assertEqual(response.status_code, 404)

    # FUNCTION TEXT
    def test_login_page_using_login_func(self):
        found = resolve('/user/login/')
        self.assertEqual(found.func, loginView)

    # TEMPLATE USE TEST
    def test_login_page_uses_template(self):
        response = Client().get('/user/login/')
        self.assertTemplateUsed(response, 'login/login.html')
    
    # TEXT TEST
    def test_login_page_content_has_content(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = loginView(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Sign In", html_response)
        # self.assertIn("password", html_response)


    # TEST FOR LOGIN 
    def test_login(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='bambang', password='12345')
        self.assertTrue(logged_in, True)

    def test_page_after_login(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='bambang', password='12345')

        response = c.get('/user/home/')
        self.assertTemplateUsed(response, 'login/home_user.html')
    
    def test_wrongpassword_after_login(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='kjadskasd', password='12345')

        response = c.get('/user/home/')
        self.assertRedirects(response, '/user/login/', status_code=302, target_status_code=200, fetch_redirect_response=True)

    def test_open_login_page(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='bambang', password='12345')

        response = c.get('/user/login/')
        self.assertRedirects(response, '/user/home/', status_code=302, target_status_code=200, fetch_redirect_response=True)
    
    def test_request_method(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        c = Client()
        # logged_in = c.login(username='bambang', password='12345')

        response = self.client.post(reverse('login'), {'username' : 'bambang', 'password' : '12345'})

        # import pdb; pdb.set_trace()
        # self.assertEqual(response.POST['username'], 'bambang')
        # self.assertEqual(response.POST['password'], '12345')

    def test_open_logout_page(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='bambang', password='12345')

        response = c.get('/user/logout/')
        self.assertTemplateUsed(response, 'login/logout.html')
    
    def test_redirect_logout_to_login_page(self):
        c = Client()
        response = c.get('/user/logout/')
        self.assertRedirects(response, '/user/login/', status_code=302, target_status_code=200, fetch_redirect_response=True)
    


