from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

def home(request):
    if request.user.is_authenticated:
        return render(request, 'login/home_user.html')
    else :
        return redirect('login')
        

def loginView(request):

    user = None

    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect('home')
        else :
            return render(request, 'login/login.html')
    
    if request.method == "POST":

        username_login = request.POST['username']
        password_login = request.POST['password']

        user = authenticate(request, username = username_login, password = password_login)

        if user is not None:
            login(request, user)
            return redirect('home')

        else :
            context = {
                'status' : 'username or password incorrect'
            }

            return render(request, 'login/login.html', context)


    return render(request, 'login/login.html')

def logoutView(request):

    if request.method == 'GET':
        if request.user.is_authenticated:
            return render(request, 'login/logout.html')
        else :
            return redirect('login')

    if request.method == 'POST':
        if (request.POST['logout'] == 'Submit'):
            logout(request)

        return redirect('login')

    return render(request, 'login/logout.html')

