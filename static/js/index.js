$(document).ready(function() {

    $body = $("body");

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=python",
        dataType: 'json',

        success: function(data) {

            var results = document.getElementById("results");
            console.log(data);

            renderBook(data);
        },


        type: 'GET',
    })

    $(document).on({
        ajaxStart: function() { $body.addClass("loading"); },
        ajaxStop: function() { $body.removeClass("loading"); }
    });

    $(".kotak-satu").click(function() {
        $("#content-satu").slideToggle("slow", function() {
            //uhuy
        });
    });

    $(".kotak-dua").click(function() {
        $("#content-dua").slideToggle("slow", function() {
            //uhuy
        });
    });

    $(".kotak-tiga").click(function() {
        $("#content-tiga").slideToggle("slow", function() {
            //uhuy
        });
    });

    $(".kotak-empat").click(function() {
        $("#content-empat").slideToggle("slow", function() {
            //uhuy
        });
    });

    $(".btn-warning").click(function() {
        $("body").toggleClass("dark-theme");
        $("p").toggleClass("p-dark-theme");
        $("h5").toggleClass("h5-dark-theme");
        $("h1").toggleClass("h1-dark-theme");
        $("h3").toggleClass("h3-dark-theme");
        $("i").toggleClass("i-dark-theme");
    });

    function renderBook(data) {
        if (data.totalItems == 0) {
            results.innerHTML += "<h3 class='maaf'>Maaf buku tidak tersedia :( </h3>";
        } else {
            console.log(data.totalItems);
            for (var i = 0; i < data.items.length; i++) {
                if (typeof(data.items[i].volumeInfo.authors == 'undefined'))
                    try {
                        if (typeof(data.items[i].volumeInfo.authors) != 'undefined') {

                            results.innerHTML += "<div class='book-card column'>" +
                                "<a href='" + data.items[i].accessInfo.webReaderLink + "' target='_blank'>" + "<img src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + " ' class='cover-buku'>" + "</a>" +
                                "<p><strong>" + data.items[i].volumeInfo.title + "</strong></p>" +
                                "<p><i>" + data.items[i].volumeInfo.authors + "</i></p> </div>";
                            var count = parseInt(data.items[i].volumeInfo.averageRating);

                            console.log(count);
                            console.log(data.items[i].volumeInfo.title);
                        } else {
                            results.innerHTML += "<div class='book-card column'>" +
                                "<a href='" + data.items[i].accessInfo.webReaderLink + "' target='_blank'>" + "<img src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + " ' class='cover-buku'>" + "</a>" +
                                "<p><strong>" + data.items[i].volumeInfo.title + "</strong></p>" +
                                "<p><i>" + "-" + "</i></p> </div>";
                            var count = parseInt(data.items[i].volumeInfo.averageRating);

                            console.log(count);
                            console.log(data.items[i].volumeInfo.title);
                        }
                    } catch (err) {
                        console.log("error euy");
                    }
            }
        }
    }

    $("#button").click(function() {
        var keyword = $("#search").val();

        $("#results").empty();

        console.log(keyword);

        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + keyword,
            dataType: 'json',

            success: function(data) {

                var results = document.getElementById("results");
                console.log(data);

                renderBook(data);
            },

            type: 'GET',
        })
    })
});