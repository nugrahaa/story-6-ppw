from django.test import TestCase
from django.conf import settings
from importlib import import_module
from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone

from .views import book

# FOR SELENIUM
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

class MyBooksMeUnitTest(TestCase):

    # URL TEST 
    def test_mybooks_page_exist(self):
        response = Client().get('/mybooks/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_found(self):
        response = Client().get('/mybooks/agan/')
        self.assertEqual(response.status_code, 404)

    # FUNCTION TEXT
    def test_mybooks_page_using_index_func(self):
        found = resolve('/mybooks/')
        self.assertEqual(found.func, book)

    # TEMPLATE USE TEST
    def test_mybooks_page_uses_template(self):
        response = Client().get('/mybooks/')
        self.assertTemplateUsed(response, 'mybooks/mybooks.html')
    
    # TEXT TEST
    def test_mybooks_page_content_has_content(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = book(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Cari Sebuah Buku", html_response)

class MyBooksFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(MyBooksFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(MyBooksFunctionalTest, self).tearDown()

    def test_search_box_title_is_exist(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/mybooks/')
        search_text = selenium.find_element_by_class_name('cari-buku-text')
        self.assertEqual(search_text.text, 'Cari Sebuah Buku')
    
    def test_search_box_is_work(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/mybooks/')
        search = selenium.find_element_by_id('search')
        button = selenium.find_element_by_id('button')

        search.send_keys('Biologi')
        time.sleep(1)
        button.send_keys(Keys.RETURN)
        time.sleep(4)
    


    