from django.test import TestCase
from django.conf import settings
from importlib import import_module
from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone

from .views import about

# FOR SELENIUM
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

class AboutMeUnitTest(TestCase):

    # URL TEST 
    def test_aboutme_page_exist(self):
        response = Client().get('/aboutme/')
        self.assertEqual(response.status_code, 200)

    def test_aboutme_url_not_found(self):
        response = Client().get('/aboutme/agan/')
        self.assertEqual(response.status_code, 404)

    # FUNCTION TEXT
    def test_aboutme_page_using_index_func(self):
        found = resolve('/aboutme/')
        self.assertEqual(found.func, about)

    # TEMPLATE USE TEST
    def test_aboutme_page_uses_template(self):
        response = Client().get('/aboutme/')
        self.assertTemplateUsed(response, 'aboutme/aboutme.html')
    
    # TEXT TEST
    def test_aboutme_page_content_has_content(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = about(request)
        html_response = response.content.decode('utf8')
        self.assertIn("About Me", html_response)

class AboutMeFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(AboutMeFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(AboutMeFunctionalTest, self).tearDown()

    # TEST FUNCTION OF ACCORDION
    def test_accordion_activity_is_working(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/aboutme/')

        response = selenium.get('http://127.0.0.1:8000/aboutme/')

        activity_accordion = selenium.find_element_by_class_name("activity-button")
        activity_accordion.click()
        time.sleep(1)

    def test_accordion_organization_is_working(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/aboutme/')

        response = selenium.get('http://127.0.0.1:8000/aboutme/')

        activity_accordion = selenium.find_element_by_class_name("organization-button")
        activity_accordion.click()
        time.sleep(1)

    def test_accordion_achievement_is_working(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/aboutme/')

        response = selenium.get('http://127.0.0.1:8000/aboutme/')

        activity_accordion = selenium.find_element_by_class_name("achievement-button")
        activity_accordion.click()
        time.sleep(1)

    def test_accordion_skill_is_working(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/aboutme/')

        response = selenium.get('http://127.0.0.1:8000/aboutme/')

        activity_accordion = selenium.find_element_by_class_name("skill-button")
        activity_accordion.click()
        time.sleep(1)
    
    def test_change_theme_is_work(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/aboutme/')

        response = selenium.get('http://127.0.0.1:8000/aboutme/')

        button = selenium.find_element_by_class_name('btn-warning')
        button.click()

        time.sleep(2)
