# Ari Angga Nugraha

## Website for Story 6 and Story 7 PPW

### Story 6 --> Testing with Unit Test and Selenium
### Story 7 --> Jquery + testing
### Story 8 --> Ajax Dynamic Data Aces
### Story 9 --> Session and Cookies (Login Feature)

### Website link :

> https://commentmee.herokuapp.com/ 


[![pipeline status](https://gitlab.com/nugrahaa/story-6-ppw/badges/master/pipeline.svg)](https://gitlab.com/nugrahaa/story-6-ppw/commits/master)
[![coverage report](https://gitlab.com/nugrahaa/story-6-ppw/badges/master/coverage.svg)](https://gitlab.com/nugrahaa/story-6-ppw/commits/master)

